package com.testapp.fashionwardrobe.wardrobe.DataClass;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.testapp.fashionwardrobe.wardrobe.Database.DatabaseHelper;
import com.testapp.fashionwardrobe.wardrobe.pojo.ViewItemsPoJo;

import java.util.ArrayList;
import java.util.List;

import static com.testapp.fashionwardrobe.wardrobe.Database.DatabaseHelper.ITEMS_TABLE_COL_ITEM_CATE01;
import static com.testapp.fashionwardrobe.wardrobe.Database.DatabaseHelper.ITEMS_TABLE_COL_ITEM_CATE02;
import static com.testapp.fashionwardrobe.wardrobe.Database.DatabaseHelper.ITEMS_TABLE_NAME;

public class mensShouesData {
    private static DatabaseHelper myDB;


    public static ArrayList<ViewItemsPoJo> getData(Context context)

    {

        myDB = new DatabaseHelper(context);
        ArrayList<ViewItemsPoJo> imageList = new ArrayList<>();
        // private List<Similarpojo> initializeList() {

        List<ViewItemsPoJo> imagers = new ArrayList<>();


        SQLiteDatabase db = myDB.getReadableDatabase();


        Cursor res = db.rawQuery("select * from "+ITEMS_TABLE_NAME+" where "+ITEMS_TABLE_COL_ITEM_CATE01+  "=? AND " +ITEMS_TABLE_COL_ITEM_CATE02+ "=? ", new String[]{"3", "3"});
//        Cursor res = db.rawQuery("select * from TABLE_ITEMS",null);

        String getImg,getName,getLoca;
        Double getLat,getLun,getPrice;
        int inte;

        String[] images = new String[res.getCount()];
        String[] name = new String[res.getCount()];
        String[] location = new String[res.getCount()];
        Double[] lat = new Double[res.getCount()];
        Double[] lun = new Double[res.getCount()];
        Double[] price = new Double[res.getCount()];

        int a = 0;
        while(res.moveToNext()){
            getImg = res.getString(res.getColumnIndex("ITEM_IMAGE"));
            getLat = res.getDouble(res.getColumnIndex("ITEM_LAT"));
            getLun = res.getDouble(res.getColumnIndex("ITEM_LUN"));
            getName = res.getString(res.getColumnIndex("ITEM_NAME"));
            getPrice = res.getDouble(res.getColumnIndex("ITEM_PRICE"));
            getLoca = res.getString(res.getColumnIndex("ITEM_LOCATION"));

            images[a] = getImg;
            lat[a] = getLat;
            lun[a] = getLun;
            name[a] = getName;
            price[a] = getPrice;
            location[a] = getLoca;

            a++;
        }


        for (int i = 0; i < images.length && i < price.length && i < name.length && i<lat.length && i<lun.length && i<location.length; i++) {

            ViewItemsPoJo mListItem = new ViewItemsPoJo();
            mListItem.image = images[i];
            mListItem.price = price[i];
            mListItem.item_name = name[i];
            mListItem.Lat=lat[i];
            mListItem.Lung=lun[i];
            mListItem.location_name=location[i];
            imageList.add(mListItem);
        }
        return imageList;
    }
}

