package com.testapp.fashionwardrobe.wardrobe.adapter;

import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.Target;
import com.testapp.fashionwardrobe.wardrobe.R;
import com.testapp.fashionwardrobe.wardrobe.pojo.ListPojo;
import com.testapp.fashionwardrobe.wardrobe.pojo.Similarpojo;
import com.testapp.fashionwardrobe.wardrobe.pojo.Similarpojo;

import java.util.List;

/**
 * Created by inovaitsys on 8/31/17.
 */

public class SimilarAdapter extends RecyclerView.Adapter<SimilarAdapter.ViewHolder> {
    Context context;
    List<Similarpojo> similarlist;
    SimilarAdapter.ViewHolder viewHolder;

    public SimilarAdapter(Context context,List<Similarpojo> similarlist) {
        this.context=context;
        this.similarlist=similarlist;

    }

    @Override
    public SimilarAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.similer_image, parent, false);
        SimilarAdapter.ViewHolder vh = new SimilarAdapter.ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(SimilarAdapter.ViewHolder holder, int position) {

        final Similarpojo similarpojo = similarlist.get(position);
      // Glide.with(context).load(similarlist.getImageUrl()).into(holder.similer_image);

      //  holder.imageUrl.setImageURI(Uri.parse(similarpojo.getImageUrl()));


        Glide.with(context)
                .load(similarpojo.getImageUrl())
                .thumbnail(0.5f)
                .crossFade()
                .placeholder(R.mipmap.ic_launcher)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(holder.imageView);

        holder.item_name.setText(similarpojo.getItem_name());
        holder.price.setText(similarpojo.getPrice());

    }

    @Override
    public int getItemCount() {
        return similarlist.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView item_name;
        TextView price;
        ImageView imageView;


        public ViewHolder(View itemView)
        {
            super(itemView);

            item_name=itemView.findViewById(R.id.item_name);
            price=itemView.findViewById(R.id.price);
            imageView=itemView.findViewById(R.id.similar_image);

        }

    }
}
