package com.testapp.fashionwardrobe.wardrobe.util;

import android.content.Intent;
import android.os.Handler;

import com.daimajia.androidanimations.library.Techniques;
import com.testapp.fashionwardrobe.wardrobe.R;
import com.testapp.fashionwardrobe.wardrobe.main.LoginActivity;
import com.viksaa.sssplash.lib.activity.AwesomeSplash;
import com.viksaa.sssplash.lib.model.ConfigSplash;

public class Splash extends AwesomeSplash {
    @Override
    public void initSplash(ConfigSplash configSplash) {
        configSplash.setBackgroundColor(R.color.colorPrimaryDark1); //any color you want form colors.xml
        //Customize Logo
        configSplash.setLogoSplash(R.drawable.checkspl);
       // configSplash.setLogoSplash(R.string.action_cart);//or any other drawable
        configSplash.setAnimLogoSplashDuration(1500 ); //int ms
      //  configSplash.setAnimLogoSplashTechnique(Techniques.SlideOutUp); //choose one form Techniques (ref: https://github.com/daimajia/AndroidViewAnimations)

        //Customize Title
        configSplash.setTitleSplash("WARDROBE");
        configSplash.setTitleTextColor(R.color.colorTextPrimary);
        configSplash.setTitleTextSize(35f); //float value
        configSplash.setAnimTitleDuration(15);
        configSplash.setAnimTitleTechnique(Techniques.FadeInUp);
    }

    @Override
    public void animationsFinished() {

        new Handler().postDelayed(new Runnable(){
            @Override
            public void run() {
                /* Create an Intent that will start the Main-Activity. */
//                Intent mainIntent = new Intent(SplashScreen.this,LoginActivity.class);
//
//                SplashScreen.this.startActivity(mainIntent);
//                finish();
 //               if (Utility.checkLogin(SplashScreen.this)) {

                Intent mainIntent = new Intent(Splash.this, LoginActivity.class);

                Splash.this.startActivity(mainIntent);
                    finish();
//                }
                //else{
//
//                    Intent mainIntent = new Intent(SplashScreen.this,LoginActivity.class);
//
//                    SplashScreen.this.startActivity(mainIntent);
//                    finish();
//
//                }
//                SplashScreen.this.finish();
            }
        }, 2000);

//        Intent mainIntent = new Intent(SplashScreen.this,LoginActivity.class);

//        SplashScreen.this.startActivity(mainIntent);

    }

}
