package com.testapp.fashionwardrobe.wardrobe.main;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.testapp.fashionwardrobe.wardrobe.R;

public class ViewItemsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_items);
    }
}
