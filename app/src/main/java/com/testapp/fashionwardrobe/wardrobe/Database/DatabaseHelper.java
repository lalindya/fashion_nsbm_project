package com.testapp.fashionwardrobe.wardrobe.Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.testapp.fashionwardrobe.wardrobe.DataClass.DatabaseData;

public class  DatabaseHelper extends SQLiteOpenHelper{
    private DatabaseData DBdata;
    private static Context context;

    //********** Strings *************

    //Databse
    public static final String DATABASE_NAME="wardrobe.db";

    //Table Login
    public static final String TABLE_LOGIN="logintable";
    public static final String COL_ID="ID";
    public static final String COL_UNAME="UNAME";
    public static final String COL_PWORD="PASSWORD";
    public static final String COL_EMAIL="EMAIL";
    public static final String COL_PHONE="PHONE";

    //Table ORDER
    public static final String ORDER_TABLE_NAME="order_table";
    public static final String ORDER_TABLE_COL_ORDER_ID="order_id";
    public static final String ORDER_TABLE_COL_ITEM_URL="image";
    public static final String ORDER_TABLE_COL_ITEM_PRICE="item_price";
    public static final String ORDER_TABLE_COL_ITEM_NAME="item_name";
    public static final String ORDER_TABLE_COL_ITEM_LOCATION="item_location";

    //Table Category 01 - Main Types
    public static final String CATE01_TABLE_NAME="TABLE_CATE01";
    public static final String CATE01_TABLE_COL_CATE01_ID="CATE01_ID";
    public static final String CATE01_TABLE_COL_CATE01_NAME="CATE01_NAME";

    //Table Category 02 - Item Types
    public static final String CATE02_TABLE_NAME="TABLE_CATE02";
    public static final String CATE02_TABLE_COL_CATE02_ID="CATE02_ID";
    public static final String CATE02_TABLE_COL_CATE02_NAME="CATE02_NAME";


    //Table Items
    public static final String ITEMS_TABLE_NAME="TABLE_ITEMS";
    public static final String ITEMS_TABLE_COL_ITEM_ID="ITEM_ID";
    public static final String ITEMS_TABLE_COL_ITEM_NAME="ITEM_NAME";
    public static final String ITEMS_TABLE_COL_ITEM_PRICE="ITEM_PRICE";
    public static final String ITEMS_TABLE_COL_ITEM_LOCATION="ITEM_LOCATION";
    public static final String ITEMS_TABLE_COL_ITEM_LAT="ITEM_LAT";
    public static final String ITEMS_TABLE_COL_ITEM_LUN="ITEM_LUN";
    public static final String ITEMS_TABLE_COL_ITEM_IMAGE="ITEM_IMAGE";
    public static final String ITEMS_TABLE_COL_ITEM_CATE01="CATE01_ID";
    public static final String ITEMS_TABLE_COL_ITEM_CATE02="CATE02_ID";


    //Create Tables Strings
    public static final String CREATE_LOGINTABLE = "CREATE TABLE IF NOT EXISTS "
            +TABLE_LOGIN+ "( "+COL_ID+" INTEGER PRIMARY KEY AUTOINCREMENT,"
            +COL_UNAME+" TEXT,"
            +COL_PWORD+" TEXT,"
            +COL_EMAIL+" TEXT,"
            +COL_PHONE+" NUMBER)";

    public static final String CREATE_ORDER_TABLE = "CREATE TABLE IF NOT EXISTS "
            + ORDER_TABLE_NAME + "("
            + ORDER_TABLE_COL_ORDER_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + ORDER_TABLE_COL_ITEM_PRICE + " DOUBLE, "
            + ORDER_TABLE_COL_ITEM_LOCATION + " TEXT, "
            + ORDER_TABLE_COL_ITEM_NAME + " TEXT, "
            + ORDER_TABLE_COL_ITEM_URL + " TEXT )";

    public static final String CREATE_CATE01_TABLE="CREATE TABLE IF NOT EXISTS "
            +CATE01_TABLE_NAME+ "( "
            +CATE01_TABLE_COL_CATE01_ID+" INTEGER PRIMARY KEY AUTOINCREMENT,"
            +CATE01_TABLE_COL_CATE01_NAME+" TEXT )";

    public static final String CREATE_CATE02_TABLE="CREATE TABLE IF NOT EXISTS "
            +CATE02_TABLE_NAME+ "( "
            +CATE02_TABLE_COL_CATE02_ID+" INTEGER PRIMARY KEY AUTOINCREMENT,"
            +CATE02_TABLE_COL_CATE02_NAME+" TEXT )";

    public static final String CREATE_ITEMS_TABLE = "CREATE TABLE IF NOT EXISTS "
            + ITEMS_TABLE_NAME + "("
            + ITEMS_TABLE_COL_ITEM_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + ITEMS_TABLE_COL_ITEM_NAME + " TEXT, "
            + ITEMS_TABLE_COL_ITEM_PRICE + " DOUBLE, "
            + ITEMS_TABLE_COL_ITEM_LOCATION + " TEXT, "
            + ITEMS_TABLE_COL_ITEM_LAT + " DOUBLE, "
            + ITEMS_TABLE_COL_ITEM_LUN + " DOUBLE, "
            + ITEMS_TABLE_COL_ITEM_IMAGE + " TEXT, "
            + ITEMS_TABLE_COL_ITEM_CATE01 + " INTEGER, "
            + ITEMS_TABLE_COL_ITEM_CATE02 + " INTEGER )";



    //************ Create Tables and DB *************

    //Create DB
    public DatabaseHelper(Context context) {
        super(context,DATABASE_NAME, null, 1);

    }


    @Override
    public void onCreate(SQLiteDatabase db) {

        //Table creation
        db.execSQL(CREATE_LOGINTABLE);
        db.execSQL(CREATE_ORDER_TABLE);
        db.execSQL(CREATE_CATE01_TABLE);
        db.execSQL(CREATE_CATE02_TABLE);
        db.execSQL(CREATE_ITEMS_TABLE);

//        DBdata = new DatabaseData(this);
//        DBdata.InsertDataIfEmpty();


    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS "+DATABASE_NAME);
    }


    //************ Insert Data Into Tables *************

    //Insert Data into Login Table
    public boolean insertData(String uname,String password, String email, int phone)
    {
        SQLiteDatabase db=this.getWritableDatabase();
        ContentValues contentvalues=new ContentValues();
        contentvalues.put(COL_UNAME,uname);
        contentvalues.put(COL_PWORD,password);
        contentvalues.put(COL_EMAIL,email);
        contentvalues.put(COL_PHONE,phone);
        long result= db.insert(TABLE_LOGIN,null,contentvalues);

        if (result == -1)
            return false;
        else
            return true;
    }

    //Insert data into Order Data Table
    public boolean insertOrderData(String item_image_url,double item_price, String item_location, String item_name)
    {
        SQLiteDatabase db=this.getWritableDatabase();
        ContentValues contentvalues=new ContentValues();
        contentvalues.put(ORDER_TABLE_COL_ITEM_URL,item_image_url);
        contentvalues.put(ORDER_TABLE_COL_ITEM_PRICE,item_price);
        contentvalues.put(ORDER_TABLE_COL_ITEM_LOCATION,item_location);
        contentvalues.put(ORDER_TABLE_COL_ITEM_NAME,item_name);
        long result= db.insert(ORDER_TABLE_NAME,null,contentvalues);

        if (result == -1)
            return false;
        else
            return true;
    }

    //Insert data into Items Table
    public boolean insertItemData(String name,double price, String location, double lat, double lun, String image, int cate01, int cate02)
    {
        SQLiteDatabase db=this.getWritableDatabase();
        ContentValues contentvalues=new ContentValues();
        contentvalues.put(ITEMS_TABLE_COL_ITEM_NAME,name);
        contentvalues.put(ITEMS_TABLE_COL_ITEM_PRICE,price);
        contentvalues.put(ITEMS_TABLE_COL_ITEM_LOCATION,location);
        contentvalues.put(ITEMS_TABLE_COL_ITEM_LAT,lat);
        contentvalues.put(ITEMS_TABLE_COL_ITEM_LUN,lun);
        contentvalues.put(ITEMS_TABLE_COL_ITEM_IMAGE,image);
        contentvalues.put(ITEMS_TABLE_COL_ITEM_CATE01,cate01);
        contentvalues.put(ITEMS_TABLE_COL_ITEM_CATE02,cate02);
        long result= db.insert(ITEMS_TABLE_NAME,null,contentvalues);

        if (result == -1)
            return false;
        else
            return true;
    }

    //Insert data into Table Category 01
    public boolean insertCate01Data(String name)
    {
        SQLiteDatabase db=this.getWritableDatabase();
        ContentValues contentvalues=new ContentValues();
        contentvalues.put(CATE01_TABLE_COL_CATE01_NAME,name);
        long result= db.insert(CATE01_TABLE_NAME,null,contentvalues);

        if (result == -1)
            return false;
        else
            return true;
    }

    //Insert data into Table Category 02
    public boolean insertCate02Data(String name)
    {
        SQLiteDatabase db=this.getWritableDatabase();
        ContentValues contentvalues=new ContentValues();
        contentvalues.put(CATE02_TABLE_COL_CATE02_NAME,name);
        long result= db.insert(CATE02_TABLE_NAME,null,contentvalues);

        if (result == -1)
            return false;
        else
            return true;
    }


    //************ Get Data From Tables *************

    //Get all data (username and password) from DB
    public Cursor getAllData()
    {
        SQLiteDatabase db=this.getWritableDatabase();
        Cursor res= db.rawQuery("select* from "+TABLE_LOGIN,null);
        return res;
    }


    //GetData Items
    public String[] getItemsDB(String field,String cat1,String cat2)
    {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select "+field+" from "+ITEMS_TABLE_NAME+" where "+ITEMS_TABLE_COL_ITEM_CATE01+  "=? AND " +ITEMS_TABLE_COL_ITEM_CATE02+ "=? ", new String[]{cat1, cat2});

        String[] array = new String[res.getCount()];
        int i = 0;
        while(res.moveToNext()){
            String text = res.getString(0);
            array[i] = text;
            i++;
        }
        return array;
    }

    public void sendinfo(){

    }


    //************ Verifications From Tables *************

    //Verify the username and password
    public boolean Verification(String username, String pw) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select * from "+TABLE_LOGIN+" where "+COL_UNAME+  "=? AND " +COL_PWORD+ "=? ", new String[]{username, pw});

        if (res.getCount() > 0) {
            return true;
        }
        else {
            return false;
        }


    }

    //Verify Data in Cate01 Table
    public boolean Cate01DataVerification() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select * from "+CATE01_TABLE_NAME,null);

        if (res.getCount() > 0) {
            return true;
        }
        else {
            return false;
        }


    }

    //Verify Data in Cate02 Table
    public boolean Cate02DataVerification() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select * from "+CATE02_TABLE_NAME,null);

        if (res.getCount() > 0) {
            return true;
        }
        else {
            return false;
        }


    }

    //Verify Data in Items Table
    public boolean ItemsDataVerification() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select * from "+ITEMS_TABLE_NAME,null);

        if (res.getCount() > 0) {
            return true;
        }
        else {
            return false;
        }


    }

}
