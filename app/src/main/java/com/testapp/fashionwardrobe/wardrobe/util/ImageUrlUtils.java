package com.testapp.fashionwardrobe.wardrobe.util;

import com.testapp.fashionwardrobe.wardrobe.pojo.ViewItemsPoJo;

import java.util.ArrayList;


public class ImageUrlUtils {
    static ArrayList<ViewItemsPoJo> cartListImageUri = new ArrayList<>();

    // Methods for Cart
    public void addCartListImageUri(ViewItemsPoJo wishlistImageUri) {
        this.cartListImageUri.add(0,wishlistImageUri);
    }

    public void removeCartListImageUri(int position) {
        this.cartListImageUri.remove(position);
    }

    public ArrayList<ViewItemsPoJo> getCartListImageUri(){ return this.cartListImageUri; }
}
