package com.testapp.fashionwardrobe.wardrobe.main;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.testapp.fashionwardrobe.wardrobe.DataClass.DatabaseData;
import com.testapp.fashionwardrobe.wardrobe.Database.DatabaseHelper;
import com.testapp.fashionwardrobe.wardrobe.R;


public class LoginActivity extends AppCompatActivity implements View.OnClickListener{
    Button facebook_button;
    TextView question;
    Button   buttonlogin;
    TextView create_account;
    private EditText username;
    private EditText password;
    ImageView visiblepass;

    private DatabaseHelper myDB;
    private static Context context;
   private static DatabaseData DBdata;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        LoginActivity.context = getApplicationContext();
//        Toolbar mToolbar = (Toolbar) findViewById(R.id.signin_toolbar1);
//        setSupportActionBar(mToolbar);
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        getSupportActionBar().setDisplayShowTitleEnabled(false);
//        mToolbar.setTitle("Sign In");

        question=(TextView)findViewById(R.id.question);
        create_account=(TextView)findViewById(R.id.create_account);
        buttonlogin=(Button)findViewById(R.id.buttonlogin);
        username=(EditText)findViewById(R.id.editTextName);
        password=(EditText)findViewById(R.id.editTextPwrd);
        visiblepass=(ImageView) findViewById(R.id.visiblepass);

        myDB = new DatabaseHelper(this);
        DBdata = new DatabaseData(this);

        create_account.setOnClickListener(this);
        buttonlogin.setOnClickListener(this);
        // instagram_button.setOnClickListener(this);
        //    getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);

        //Insert Data into DB if not available
        DBdata.InsertDataIfEmpty(context);

        //visibilitiy password on click of image view
        visiblepass.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                password.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);

            }
        });



    }


    @Override
    public void onClick(View view) {

        switch (view.getId()){


            case R.id.create_account:
                Intent i = new Intent(LoginActivity.this, RegistrationActivity.class);
                startActivity(i);
                break;

            case R.id.buttonlogin:
                getVerification();
                break;
        }
    }

    //Verify the username and password
    public void getVerification() {

        if(TextUtils.isEmpty(username.getText()))
        {
            username.setError("Enter Username");
            return;
        }
        else if(TextUtils.isEmpty(password.getText()))
        {
            password.setError("Enter Password");
            return;
        }
        else {
            boolean getv = myDB.Verification(username.getText().toString(), password.getText().toString());
            if (getv == true) {
                Toast toast = Toast.makeText(getApplicationContext(), "Login Success!", Toast.LENGTH_LONG);
                toast.show();
                Intent intent = new Intent(LoginActivity.this, CategoriesActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            } else
                showMessage("Login", "Incorrect Username or Password!");
        }
    }

    //Common Dialog Box
    public void showMessage(String title, String Message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);
        builder.setTitle(title);
        builder.setMessage(Message);
        builder.show();
    }

}