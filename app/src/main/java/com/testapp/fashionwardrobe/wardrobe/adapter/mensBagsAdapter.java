package com.testapp.fashionwardrobe.wardrobe.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.testapp.fashionwardrobe.wardrobe.R;
import com.testapp.fashionwardrobe.wardrobe.main.ItemDetailsActivity;
import com.testapp.fashionwardrobe.wardrobe.pojo.ViewItemsPoJo;

import java.util.List;

public class mensBagsAdapter extends RecyclerView.Adapter<mensBagsAdapter.ViewHolder> {

    Context context;
    List<ViewItemsPoJo> viewItemsPoJoList;
    KidsTshirtAdapter.ViewHolder viewHolder;

    public mensBagsAdapter(Context context,List<ViewItemsPoJo> viewItemsPoJoList) {
        this.context=context;
        this.viewItemsPoJoList=viewItemsPoJoList;

    }
    @Override
    public mensBagsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.viewitemlayout, parent, false);
        mensBagsAdapter.ViewHolder vh = new mensBagsAdapter.ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(mensBagsAdapter.ViewHolder holder, int position) {
        final ViewItemsPoJo viewItemsPoJo = viewItemsPoJoList.get(position);

        Glide.with(context)
                .load(viewItemsPoJo.getImage())
                .thumbnail(0.5f)
                .crossFade()
                .placeholder(R.mipmap.ic_launcher)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(holder.displayimage);

        holder.item_name.setText(viewItemsPoJo.getItem_name());
        holder.price.setText(viewItemsPoJo.getPrice().toString());
    }

    @Override
    public int getItemCount() {
        return viewItemsPoJoList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        TextView item_name;
        TextView price;
        ImageView displayimage;


        public ViewHolder(View itemView)
        {
            super(itemView);
            itemView.setOnClickListener(this);
            item_name=itemView.findViewById(R.id.item_name);
            price=itemView.findViewById(R.id.price);
            displayimage=itemView.findViewById(R.id.displayimage);

        }

        @Override
        public void onClick(View view) {
            int position = getAdapterPosition();
            ViewItemsPoJo viewItemsPoJo = new ViewItemsPoJo();
            viewItemsPoJo = viewItemsPoJoList.get(position);

            Log.d("position", String.valueOf(position) + "++++++++++++++++");

            Intent intent = new Intent(context, ItemDetailsActivity.class);


            Bundle bundle = new Bundle();
            bundle.putSerializable("value", viewItemsPoJo);
            intent.putExtras(bundle);
            context.startActivity(intent);
        }
    }
}
