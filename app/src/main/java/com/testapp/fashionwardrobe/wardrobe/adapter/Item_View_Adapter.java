package com.testapp.fashionwardrobe.wardrobe.adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.testapp.fashionwardrobe.wardrobe.R;

/**
 * Created by inovaitsys on 9/1/17.
 */

public class Item_View_Adapter extends PagerAdapter {
     ImageView imageView;
    private int[] imgs = {R.drawable.hat_a, R.drawable.hat, R.drawable.imagesb, R.drawable.imagesb};
    private LayoutInflater inflater;
    private Context ctx;

   public Item_View_Adapter(Context ctx){

       this.ctx=ctx;
   }
    @Override
    public int getCount() {
        return imgs.length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {

        inflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View item_view = inflater.inflate(R.layout.view, container, false);
        ImageView imageView = (ImageView) item_view.findViewById(R.id.item_face);
        imageView.setImageResource(imgs[position]);
        container.addView(item_view);
        return item_view;
    }

    @Override

    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }
}
