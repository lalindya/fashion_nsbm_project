package com.testapp.fashionwardrobe.wardrobe.pojo;



public class Similarpojo {
    public int image;
    public String item_name;
    public String price;


    public int getImageUrl() {
        return image;
    }

    public void setImageUrl(int imageUrl) {
        this.image = imageUrl;
    }

    public String getItem_name() {
        return item_name;
    }

    public void setItem_name(String item_name) {
        this.item_name = item_name;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }


}
