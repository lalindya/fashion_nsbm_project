package com.testapp.fashionwardrobe.wardrobe.main;

import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.testapp.fashionwardrobe.wardrobe.R;
import com.testapp.fashionwardrobe.wardrobe.adapter.Item_View_Adapter;
import com.testapp.fashionwardrobe.wardrobe.adapter.ViewPagerAdapter;

import me.relex.circleindicator.CircleIndicator;

public class ItemViewActivity extends AppCompatActivity {
    ViewPager viewPager;
    Item_View_Adapter adapter;
    private static int currentpage = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_view);

        viewPager = (ViewPager) findViewById(R.id.item_view_page);
        adapter = new Item_View_Adapter(this);
        viewPager.setAdapter(adapter);

        CircleIndicator indicator1 = (CircleIndicator) findViewById(R.id.view_indicator);
          indicator1.setViewPager(viewPager);
     //   viewPager.setCurrentItem(0);

    }
}
