package com.testapp.fashionwardrobe.wardrobe.main;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.MediaRouteButton;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.facebook.drawee.view.SimpleDraweeView;
import com.testapp.fashionwardrobe.wardrobe.Database.DatabaseHelper;
import com.testapp.fashionwardrobe.wardrobe.R;
import com.testapp.fashionwardrobe.wardrobe.pojo.ViewItemsPoJo;
import com.testapp.fashionwardrobe.wardrobe.util.ImageUrlUtils;
import com.yarolegovich.lovelydialog.LovelyStandardDialog;

import java.util.ArrayList;

public class CartListActivity extends AppCompatActivity implements View.OnClickListener {

    public static Context mContext;
    private ArrayList<ViewItemsPoJo> cartlistImageUri;
    private Double payment;
    private  TextView place_order_btn;
    private DatabaseHelper myDB;
    private SimpleStringRecyclerViewAdapter adapter;
    private TextView paymenttxt;
    private LinearLayout layoutCartItems;
    private LinearLayout layoutCartNoItems;
    private LinearLayout layoutCartPayments;
    private Button bStartShopping;

    @SuppressLint("WrongViewCast")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);

        mContext = CartListActivity.this;
        myDB = new DatabaseHelper(this);

        ImageUrlUtils imageUrlUtils = new ImageUrlUtils();
        cartlistImageUri =imageUrlUtils.getCartListImageUri();
        //Show cart layout based on items
        setCartLayout();

        RecyclerView recyclerView = (RecyclerView)findViewById(R.id.recyclerview);
        RecyclerView.LayoutManager recylerViewLayoutManager = new LinearLayoutManager(this);

        recyclerView.setLayoutManager(recylerViewLayoutManager);
        adapter = new CartListActivity.SimpleStringRecyclerViewAdapter(recyclerView, cartlistImageUri);
        recyclerView.setAdapter(adapter);
        place_order_btn=(TextView)findViewById(R.id.place_order_btn);

        place_order_btn.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.place_order_btn:
                for(int i=0;i<cartlistImageUri.size();i++){
                    boolean res = myDB.insertOrderData(cartlistImageUri.get(i).getImage().toString(), cartlistImageUri.get(i).getPrice(),cartlistImageUri.get(i).getLocation_name().toString(),cartlistImageUri.get(i).getItem_name().toString());
                    if (res == true)
                    {
//                        AlertDialog.Builder dialog = new AlertDialog.Builder(CartListActivity.this);
//                        dialog.setCancelable(false);
//                        dialog.setTitle(" wardrobe ");
//                        dialog.setMessage("You have been successfully place an order your toatal bill is Rs. "+ payment  );
//                        dialog.setNegativeButton(" OK ", new DialogInterface.OnClickListener() {
//
//                            @Override
//                            public void onClick(DialogInterface dialog, int which) {
//                                //Action for "Cancel".
//                                dialog.dismiss();
//                            }
//                        });
//
//                        final AlertDialog alert = dialog.create();
//                        alert.show();
                        new LovelyStandardDialog(this, LovelyStandardDialog.ButtonLayout.VERTICAL)
                                .setTopColorRes(R.color.addtocart)
                                .setButtonsColorRes(R.color.album_title)
                                .setIcon(R.drawable.ic_shopping_cart_white_24dp)
                                .setTitle("WARDROBE")
                                .setMessage("Your Total Amount is Rs."+ payment +"\n" +"Items will be delivered within 7 days." +"\n" +
                                        "Extra charge 500/=. " +"\n"+"\n"+
                                        "Do you wish to deliver the items?")
                                .setPositiveButton("Yes", new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        Intent intent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                                                "mailto", "wardrobe.srilanka@gmail.com", null));
                                        intent.putExtra(Intent.EXTRA_SUBJECT, "Delivery - WARDROBE");
                                        intent.putExtra(Intent.EXTRA_TEXT, "MY DELIVERY ADDRESS IS -        "+"\n"+"\n"+"I have successfully placed my order and I request you to deliver my items." +
                                                "\n"+" I agree to pay an extra charge of 500/= for delivery." +
                                                "\n"+  "\n"+"Thank you");
                                        startActivity(Intent.createChooser(intent, "Choose an Email client :"));
                                    }
                                })
                                .setNegativeButton(android.R.string.no, null)
                                .show();
//                        Toast toast = Toast.makeText(getApplicationContext(), "place an order!", Toast.LENGTH_LONG);
//                        toast.show();

                        cartlistImageUri.clear();
                        adapter.notifyDataSetChanged();
                        CategoriesActivity.notificationCountCart = 0;
                        setCartLayout();


                    }
                    else if (res == false)
                    {
                        Toast toast = Toast.makeText(getApplicationContext(), "Error", Toast.LENGTH_LONG);
                        toast.show();
                    }
                }

                break;
        }
    }

    public class SimpleStringRecyclerViewAdapter
            extends RecyclerView.Adapter<CartListActivity.SimpleStringRecyclerViewAdapter.ViewHolder> {

        private ArrayList<ViewItemsPoJo> mCartlistImageUri;
        private RecyclerView mRecyclerView;

        public class ViewHolder extends RecyclerView.ViewHolder {
            public final View mView;
            public final ImageView mImageView;
            public final TextView price,location,name;
            public final LinearLayout mLayoutItem, mLayoutRemove , mLayoutEdit;

            public ViewHolder(View view) {
                super(view);
                mView = view;
                mImageView = (ImageView) view.findViewById(R.id.image_cartlist);
                mLayoutItem = (LinearLayout) view.findViewById(R.id.layout_item_desc);
                mLayoutRemove = (LinearLayout) view.findViewById(R.id.layout_action1);
                mLayoutEdit = (LinearLayout) view.findViewById(R.id.layout_action2);
                price = view.findViewById(R.id.price);
                location = view.findViewById(R.id.location);
                name = view.findViewById(R.id.item_name);
            }
        }

        public SimpleStringRecyclerViewAdapter(RecyclerView recyclerView, ArrayList<ViewItemsPoJo> wishlistImageUri) {
            mCartlistImageUri = wishlistImageUri;
            mRecyclerView = recyclerView;
        }

        @Override
        public CartListActivity.SimpleStringRecyclerViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_cartlist_item, parent, false);
            return new CartListActivity.SimpleStringRecyclerViewAdapter.ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(final CartListActivity.SimpleStringRecyclerViewAdapter.ViewHolder holder, final int position) {
            final ViewItemsPoJo item = mCartlistImageUri.get(position);
            final Uri uri = Uri.parse(item.getImage());
            // holder.mImageView.setImageURI(uri);

            Glide.with(mContext)
                    .load(item.getImage())
                    .thumbnail(0.5f)
                    .crossFade()
                    .placeholder(R.mipmap.ic_launcher)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(holder.mImageView);

            holder.price.setText(String.valueOf(item.getPrice()));
            holder.location.setText(item.getLocation_name());
            holder.name.setText(item.getItem_name());

            //Set click action
            holder.mLayoutRemove.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ImageUrlUtils imageUrlUtils = new ImageUrlUtils();
                    imageUrlUtils.removeCartListImageUri(position);
                    notifyDataSetChanged();
                    //Decrease notification count
                    CategoriesActivity.notificationCountCart--;
                    setCartLayout();

                }
            });

            //Set click action
            holder.mLayoutEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                }
            });
        }

        @Override
        public int getItemCount() {
            return mCartlistImageUri.size();
        }
    }

    public void setCartLayout(){
        layoutCartItems = (LinearLayout) findViewById(R.id.layout_items);
        layoutCartPayments = (LinearLayout) findViewById(R.id.layout_payment);
        layoutCartNoItems = (LinearLayout) findViewById(R.id.layout_cart_empty);
        paymenttxt = (TextView) findViewById(R.id.text_action_bottom1);

        payment = 0.0;
        if(CategoriesActivity.notificationCountCart >0){

            for(int i=0;i<cartlistImageUri.size();i++){
                payment += cartlistImageUri.get(i).getPrice();
            }
            paymenttxt.setText("Rs."+payment.toString());
            layoutCartNoItems.setVisibility(View.GONE);
            layoutCartItems.setVisibility(View.VISIBLE);
            layoutCartPayments.setVisibility(View.VISIBLE);
        }else {
            layoutCartNoItems.setVisibility(View.VISIBLE);
            layoutCartItems.setVisibility(View.GONE);
            layoutCartPayments.setVisibility(View.GONE);

            bStartShopping = (Button) findViewById(R.id.bAddNew);
            bStartShopping.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent categoriesIntent = new Intent(CartListActivity.mContext,CategoriesActivity.class);
                    mContext.startActivity(categoriesIntent);
                }
            });
        }
    }


}