package com.testapp.fashionwardrobe.wardrobe.main.men;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;


import com.testapp.fashionwardrobe.wardrobe.DataClass.mensBagsData;
import com.testapp.fashionwardrobe.wardrobe.DataClass.mensShouesData;
import com.testapp.fashionwardrobe.wardrobe.DataClass.mensTshirtData;
import com.testapp.fashionwardrobe.wardrobe.R;
import com.testapp.fashionwardrobe.wardrobe.adapter.mensBagsAdapter;
import com.testapp.fashionwardrobe.wardrobe.adapter.mensShouesAdapter;
import com.testapp.fashionwardrobe.wardrobe.adapter.mensTshirtAdapter;
import com.testapp.fashionwardrobe.wardrobe.pojo.ViewItemsPoJo;

import java.util.List;

public class menitemActivity extends AppCompatActivity {
    public List<ViewItemsPoJo> viewItemsPoJoList;
    RecyclerView recyclerView;
    RecyclerView mens_shouesrecyclerview;
    RecyclerView mens_bagsrecycleview;
    RecyclerView.LayoutManager layoutManager;
    mensTshirtAdapter mensTshirtAdapter;
    mensBagsAdapter mensBagsAdapter;
    mensShouesAdapter mensShouesAdapter;

    private static Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        menitemActivity.context = getApplicationContext();
        setContentView(R.layout.activity_menitem);


        recyclerView = (RecyclerView) findViewById(R.id.mens_tshirt);
        mensTshirtAdapter = new mensTshirtAdapter(this, mensTshirtData.getData(context));
        recyclerView.setAdapter(mensTshirtAdapter);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(mLayoutManager);

        mens_bagsrecycleview = (RecyclerView) findViewById(R.id.mens_bagsrecycleview);
        mensBagsAdapter = new mensBagsAdapter(this, mensBagsData.getData(context));
        mens_bagsrecycleview.setAdapter(mensBagsAdapter);
        RecyclerView.LayoutManager aLayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
        mens_bagsrecycleview.setLayoutManager(aLayoutManager);

        mens_shouesrecyclerview = (RecyclerView) findViewById(R.id.mens_shouesrecyclerview);
       mensShouesAdapter = new mensShouesAdapter(this, mensShouesData.getData(context));
        mens_shouesrecyclerview.setAdapter(mensShouesAdapter);
        RecyclerView.LayoutManager bLayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
        mens_shouesrecyclerview.setLayoutManager(bLayoutManager);

    }
}
