package com.testapp.fashionwardrobe.wardrobe.main.Women;


import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;


import com.testapp.fashionwardrobe.wardrobe.DataClass.womenBagsData;
import com.testapp.fashionwardrobe.wardrobe.DataClass.womenShouesData;
import com.testapp.fashionwardrobe.wardrobe.DataClass.womenFrocksData;
import com.testapp.fashionwardrobe.wardrobe.R;
import com.testapp.fashionwardrobe.wardrobe.adapter.womenBagsAdapter;
import com.testapp.fashionwardrobe.wardrobe.adapter.womenShouesAdapter;
import com.testapp.fashionwardrobe.wardrobe.adapter.womenFrocksAdapter;
import com.testapp.fashionwardrobe.wardrobe.pojo.ViewItemsPoJo;

import java.util.List;

public class womenitemActivity extends AppCompatActivity {
    public List<ViewItemsPoJo> viewItemsPoJoList;
    RecyclerView recyclerView;
    RecyclerView women_shouesrecyclerview;
    RecyclerView women_bagsrecycleview;
    RecyclerView.LayoutManager layoutManager;
    womenFrocksAdapter womenFrocksAdapter;
    womenBagsAdapter womenBagsAdapter;
    womenShouesAdapter womenShouesAdapter;

    private static Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        com.testapp.fashionwardrobe.wardrobe.main.Women.womenitemActivity.context = getApplicationContext();
        setContentView(R.layout.activity_womenitem);


        recyclerView = (RecyclerView) findViewById(R.id.women_frocks);
        womenFrocksAdapter = new womenFrocksAdapter(this, womenFrocksData.getData(context));
        recyclerView.setAdapter(womenFrocksAdapter);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(mLayoutManager);

        women_bagsrecycleview = (RecyclerView) findViewById(R.id.women_bagsrecycleview);
        womenBagsAdapter = new womenBagsAdapter(this, womenBagsData.getData(context));
        women_bagsrecycleview.setAdapter(womenBagsAdapter);
        RecyclerView.LayoutManager aLayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
        women_bagsrecycleview.setLayoutManager(aLayoutManager);

        women_shouesrecyclerview = (RecyclerView) findViewById(R.id.women_shouesrecyclerview);
        womenShouesAdapter = new womenShouesAdapter(this, womenShouesData.getData(context));
        women_shouesrecyclerview.setAdapter(womenShouesAdapter);
        RecyclerView.LayoutManager bLayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
        women_shouesrecyclerview.setLayoutManager(bLayoutManager);

    }
}
