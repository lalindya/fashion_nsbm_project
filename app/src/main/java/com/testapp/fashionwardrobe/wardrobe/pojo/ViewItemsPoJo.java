package com.testapp.fashionwardrobe.wardrobe.pojo;

import java.io.Serializable;

public class ViewItemsPoJo implements Serializable {


    public String image;
    public String item_name;
    public Double price;
    public String location_name;
    public Double Lat;
    public Double Lung;

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getImageUrl() {

        return image;
    }


    public String getItem_name() {
        return item_name;
    }

    public void setItem_name(String item_name) {
        this.item_name = item_name;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }
    public String getLocation_name() {
        return location_name;
    }

    public void setLocation_name(String location_name) {
        this.location_name = location_name;
    }

    public double getLat() {
        return Lat;
    }

    public void setLat(double lat) {
        Lat = lat;
    }

    public double getLung() {
        return Lung;
    }

    public void setLung(double lung) {
        Lung = lung;
    }
}
