package com.testapp.fashionwardrobe.wardrobe.main.Kids;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.testapp.fashionwardrobe.wardrobe.DataClass.KidsShouesData;
import com.testapp.fashionwardrobe.wardrobe.DataClass.KidsTrousersData;
import com.testapp.fashionwardrobe.wardrobe.DataClass.KidsTshirtData;
import com.testapp.fashionwardrobe.wardrobe.Database.DatabaseHelper;
import com.testapp.fashionwardrobe.wardrobe.R;
import com.testapp.fashionwardrobe.wardrobe.adapter.KidsTrousersDataAdapter;
import com.testapp.fashionwardrobe.wardrobe.adapter.KidsShouesAdapter;
import com.testapp.fashionwardrobe.wardrobe.adapter.KidsTshirtAdapter;
import com.testapp.fashionwardrobe.wardrobe.pojo.ViewItemsPoJo;

import java.util.List;

public class KidsItemActivity extends AppCompatActivity {
    public  List<ViewItemsPoJo> viewItemsPoJoList;
    RecyclerView recyclerView;
    RecyclerView kids_shouesrecyclerview;
    RecyclerView kids_Trousersrecyclerview;
    RecyclerView.LayoutManager layoutManager;
    KidsTshirtAdapter kidsTshirtAdapter;
    KidsTrousersDataAdapter kidsTrousersDataAdapter;
    KidsShouesAdapter kidsShouesAdapter;

    private static Context context;
    private static DatabaseHelper myDB;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        KidsItemActivity.context = getApplicationContext();
        setContentView(R.layout.activity_kids_item);
        myDB = new DatabaseHelper(this);

        recyclerView = (RecyclerView) findViewById(R.id.kids_tshirt);
        kidsTshirtAdapter = new KidsTshirtAdapter(this, KidsTshirtData.getData(context));
        recyclerView.setAdapter(kidsTshirtAdapter);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(mLayoutManager);

        kids_Trousersrecyclerview = (RecyclerView) findViewById(R.id.kids_Trousersrecyclerview);
        kidsTrousersDataAdapter = new KidsTrousersDataAdapter(this, KidsTrousersData.getData(context));
        kids_Trousersrecyclerview.setAdapter(kidsTrousersDataAdapter);
        RecyclerView.LayoutManager aLayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
        kids_Trousersrecyclerview.setLayoutManager(aLayoutManager);

        kids_shouesrecyclerview = (RecyclerView) findViewById(R.id.kids_shouesrecyclerview);
        kidsShouesAdapter = new KidsShouesAdapter(this, KidsShouesData.getData(context));
        kids_shouesrecyclerview.setAdapter(kidsShouesAdapter);
        RecyclerView.LayoutManager bLayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
        kids_shouesrecyclerview.setLayoutManager(bLayoutManager);
    }
}
