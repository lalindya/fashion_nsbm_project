package com.testapp.fashionwardrobe.wardrobe.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.testapp.fashionwardrobe.wardrobe.R;
import com.testapp.fashionwardrobe.wardrobe.adapter.CategoriesAdapter;
import com.testapp.fashionwardrobe.wardrobe.pojo.ListPojo;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class CategoriesFragment extends Fragment {

    public List<ListPojo> categorytList;
    RecyclerView recyclerView;
    RecyclerView.LayoutManager layoutManager;
    CategoriesAdapter categoriesAdapter;
    public static CategoriesFragment newInstatnce(){
        return new CategoriesFragment();
    }
    public CategoriesFragment() {
        // Required empty public constructor
    }
    public static CategoriesFragment newInstance() {
        return new CategoriesFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view =inflater.inflate(R.layout.fragment_categories, container, false);
        initializeList();
        recyclerView = (RecyclerView) view.findViewById(R.id.categorylist_recyclerview);
        layoutManager = new LinearLayoutManager(getActivity(),LinearLayoutManager.HORIZONTAL,false);
        recyclerView.setLayoutManager(layoutManager);
        categoriesAdapter = new CategoriesAdapter(getActivity(),categorytList);

        recyclerView.setAdapter(categoriesAdapter);
//        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowTitleEnabled(false);
        return view;
    }

    private void initializeList() {
        categorytList = new ArrayList<>();
        ListPojo Listpojo1 = new ListPojo();
        Listpojo1.setImageUrl("http://981thehawk.com/files/2013/01/R.-Nelson.jpg?w=600&h=0&zc=1&s=0&a=t&q=89");
        Listpojo1.setCategory_name("KIDS");

        ListPojo Listpojo2 = new ListPojo();
        Listpojo2.setImageUrl("https://cdn.wallpapershdin.com/walls/women-scarlett-johansson-celebrity-grayscale-black-background-wide.jpg");
        Listpojo2.setCategory_name("WOMEN");

        ListPojo Listpojo3 = new ListPojo();
        Listpojo3.setImageUrl("https://cdn-img.essence.com/sites/default/files/styles/pronto_original/public/1486404885/445A1467.jpg?itok=dJ_yuzND");
        Listpojo3.setCategory_name("MEN");

        categorytList.add(Listpojo1);
        categorytList.add(Listpojo2);
        categorytList.add(Listpojo3);
    }
}
