package com.testapp.fashionwardrobe.wardrobe.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.testapp.fashionwardrobe.wardrobe.R;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by inovaitsys on 8/31/17.
 */

public class CustomSpineerAdaper extends ArrayAdapter<String> {

    Context context;
    String[] colorname;
    int[] colorimage;

    public CustomSpineerAdaper(Context context, String[] colorname, int[] colorimage) {

        super(context, R.layout.color_and_size, colorname);
        this.context = context;
        this.colorname = colorname;
        this.colorimage = colorimage;
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflayer = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflayer.inflate(R.layout.color_and_size, null);

            TextView coloe_name = (TextView) convertView.findViewById(R.id.coloe_name);
            CircleImageView color_image = (CircleImageView) convertView.findViewById(R.id.colorimage);

            coloe_name.setText(colorname[position]);
            color_image.setImageResource(colorimage[position]);
        }

        return convertView;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflayer = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflayer.inflate(R.layout.color_and_size, null);

            TextView coloe_name = (TextView) convertView.findViewById(R.id.coloe_name);
            CircleImageView color_image = (CircleImageView) convertView.findViewById(R.id.colorimage);

            coloe_name.setText(colorname[position]);
            color_image.setImageResource(colorimage[position]);
        }

        return convertView;
    }
}
