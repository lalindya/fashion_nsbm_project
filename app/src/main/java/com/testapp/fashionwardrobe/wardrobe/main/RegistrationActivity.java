package com.testapp.fashionwardrobe.wardrobe.main;

import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.testapp.fashionwardrobe.wardrobe.Database.DatabaseHelper;
import com.testapp.fashionwardrobe.wardrobe.R;
public class RegistrationActivity extends AppCompatActivity implements View.OnClickListener {
TextView txtsignin;
    private EditText username;
    private EditText email;
    private Button btnReg;
    private EditText phone;
    private EditText password;
    private DatabaseHelper myDB;
    String emailText,mobile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        txtsignin=(TextView)findViewById(R.id.txtsignin);
        btnReg=(Button)findViewById(R.id.buttonRegister);
        username=(EditText) findViewById(R.id.editTextName);
        email=(EditText) findViewById(R.id.editTextEmail);
        phone=(EditText) findViewById(R.id.editTextPhone);
        password=(EditText) findViewById(R.id.editTextPwrd);
        myDB = new DatabaseHelper(this);

        txtsignin.setOnClickListener(this);
        btnReg.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.txtsignin:
                Intent i = new Intent(RegistrationActivity.this, LoginActivity.class);
                startActivity(i);
                break;

            case R.id.buttonRegister:
                userReg();
                break;

        }


    }

    //Register user (Save Data)
    public void userReg()
    {
        if(TextUtils.isEmpty(username.getText()))
        {
            username.setError("Enter Username");
            return;
        }


        emailText = email.getText().toString();
        if (!emailText.matches("[a-zA-Z0-9._-]+@[a-z]+.[a-z]+")) {
            email.setError("Invalid Email Address");
            return;

        }

        if (phone.getText().toString().trim().length() != 10)
        {
            phone.setError("Only 10 Digits Allowed!");
            return;
        }




        else if (TextUtils.isEmpty(password.getText()))
        {
            password.setError("Enter Password");
            return;
        }

        else if(TextUtils.isEmpty(email.getText()))
        {
            email.setError("Enter Email");
            return;
        }

        else if(TextUtils.isEmpty(phone.getText()))
        {
            phone.setError("Enter Phone Number");
            return;
        }

        else {
            boolean res = myDB.insertData(username.getText().toString(), password.getText().toString(),email.getText().toString(),Integer.valueOf(phone.getText().toString()));
            if (res == true)
            {
                Toast toast = Toast.makeText(getApplicationContext(), "Registered Successfully!", Toast.LENGTH_LONG);
                toast.show();
                Intent i = new Intent(RegistrationActivity.this, LoginActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(i);
            }

            else if (res == false)
            {
                Toast toast = Toast.makeText(getApplicationContext(), "Registration Error", Toast.LENGTH_LONG);
                toast.show();
            }
        }

    }

    //Common Dialog Box
    public void showMessage(String title, String Message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);
        builder.setTitle(title);
        builder.setMessage(Message);
        builder.show();

    }

    public void fieldValid()
    {
        if(TextUtils.isEmpty(username.getText())) {
            username.setError("Enter Username");
            return;
        }
    }
}
