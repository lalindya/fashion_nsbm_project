package com.testapp.fashionwardrobe.wardrobe.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.testapp.fashionwardrobe.wardrobe.R;
import com.testapp.fashionwardrobe.wardrobe.pojo.Similarpojo;

import java.util.List;

/**
 * Created by inovaitsys on 8/31/17.
 */

public class TestAdapter extends  RecyclerView.Adapter<TestAdapter.MyViewHolder> {

    private List<Similarpojo> imageList;
    Context context;
    LayoutInflater inflater;


    public TestAdapter(Context context, List<Similarpojo> imageList)
    {
        this.context = context;
        this.imageList  = imageList;
        inflater = LayoutInflater.from(context);
    }

//    public class MyViewHolder extends RecyclerView.ViewHolder {
//        public TextView item_name, price;
//        ImageView item_image;

    @Override
    public TestAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int position)
    {
        View view  = inflater.inflate(R.layout.similer_image, parent, false);

        MyViewHolder holder = new MyViewHolder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder myViewholder, int position)
    {

        myViewholder.price.setText(imageList.get(position).price);

        myViewholder.image.setImageResource(imageList.get(position).image);
        myViewholder.item_name.setText(imageList.get(position).item_name);

    }

    @Override
    public int getItemCount()
    {
        return imageList.size();

    }

//        public MyViewHolder(View view) {
//            super(view);
//            item_name = (TextView) view.findViewById(R.id.item_name);
//            price = (TextView) view.findViewById(R.id.price);
//            item_image = (ImageView) view.findViewById(R.id.similar_image);
//        }
//    }

//    public TestAdapter(List<Similarpojo> imageList) {
//        this.imageList = imageList;
//    }
//
//    @Override
//    public TestAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
//        View itemView = LayoutInflater.from(parent.getContext())
//                .inflate(R.layout.similer_image, parent, false);
//
//        return new MyViewHolder(itemView);
//    }
//
//    @Override
//    public void onBindViewHolder(TestAdapter.MyViewHolder holder, int position) {
//        Similarpojo movie = imageList.get(position);
//        holder.item_name.setText(movie.getItem_name());
//        holder.price.setText(movie.getPrice());
//        holder.item_image.setImageResource(Integer.parseInt(movie.getItem_name()));
//    }
//
//    @Override
//    public int getItemCount() {
//        return imageList.size();
//    }
//
class MyViewHolder extends RecyclerView.ViewHolder
{

    TextView item_name;
    ImageView image;
    TextView price;



    public MyViewHolder(View itemView) {
        super(itemView);


        item_name = itemView.findViewById(R.id.item_name);
        image = itemView.findViewById(R.id.similar_image);
        price=itemView.findViewById(R.id.price);




    }



}

}


