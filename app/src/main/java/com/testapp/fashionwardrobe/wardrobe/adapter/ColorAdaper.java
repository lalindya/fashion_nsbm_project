package com.testapp.fashionwardrobe.wardrobe.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.testapp.fashionwardrobe.wardrobe.R;
import com.testapp.fashionwardrobe.wardrobe.pojo.ColorPojo;
import com.testapp.fashionwardrobe.wardrobe.pojo.ListPojo;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by inovaitsys on 8/30/17.
 */

public class ColorAdaper extends RecyclerView.Adapter<ColorAdaper.ViewHolder> {

    Context context;
    List<ColorPojo> colorPojoList;
    ColorAdaper.ViewHolder viewHolder;

    ArrayList item_list = new ArrayList<>();

    public ColorAdaper(Context context, List<ColorPojo> productList) {

        this.context = context;
        this.colorPojoList = productList;
    }

    @Override
    public ColorAdaper.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.color, parent, false);
        ColorAdaper.ViewHolder vh = new ColorAdaper.ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ColorAdaper.ViewHolder holder, int position) {

//        final ColorPojo ColorPojo = colorPojoList.get(position);
//        holder.id.setBackground(Integer.parseInt(ColorPojo.getColor()));
    }

    @Override
    public int getItemCount() {
        return colorPojoList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        //View color;
        View id;

        public ViewHolder(View itemView) {

            super(itemView);
            //color = itemView.findViewById(R.id.color_view);
        id=itemView.findViewById(R.id.color_view);
        }
    }
}
