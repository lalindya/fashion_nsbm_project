package com.testapp.fashionwardrobe.wardrobe.main;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.daimajia.slider.library.Tricks.ViewPagerEx;
import com.testapp.fashionwardrobe.wardrobe.DataClass.Data;
import com.testapp.fashionwardrobe.wardrobe.R;
import com.testapp.fashionwardrobe.wardrobe.adapter.CustomSpineerAdaper;
import com.testapp.fashionwardrobe.wardrobe.adapter.SimilarAdapter;
import com.testapp.fashionwardrobe.wardrobe.adapter.TestAdapter;
import com.testapp.fashionwardrobe.wardrobe.main.Kids.KidsItemActivity;
import com.testapp.fashionwardrobe.wardrobe.pojo.Similarpojo;
import com.testapp.fashionwardrobe.wardrobe.pojo.ViewItemsPoJo;
import com.testapp.fashionwardrobe.wardrobe.util.ImageUrlUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import me.relex.circleindicator.CircleIndicator;

public class ItemDetailsActivity extends AppCompatActivity implements View.OnClickListener {

    public static final String EXTRA_POSITION = "position";
    SliderLayout mDemoSlider;
    ImageView imageView;
    RecyclerView recyclerView;
    TestAdapter adapter;

    RecyclerView.LayoutManager layoutManager;

    public List<Similarpojo> similarlist;
    Context context;

    private ImageView displayimage;
    private TextView textView_name;
    private TextView textView_price;
    private TextView locations;
    private Button getLocationBtn;
    private ViewItemsPoJo viewItemsPoJo;
    private Button addtocartbtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Spinner size_spinner;
        Spinner color_spinner;
        int spinner_position;
        SliderLayout sliderLayout;
        String[] color;

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_details);

////        recyclerView = (RecyclerView) findViewById(R.id.similer);
//        adapter = new TestAdapter(this, Data.getData());
//        recyclerView.setAdapter(adapter);
//        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
//        recyclerView.setLayoutManager(mLayoutManager);

//        recyclerView.setLayoutManager(mLayoutManager);
//        recyclerView.setItemAnimator(new DefaultItemAnimator());
        // recyclerView.setAdapter(testAdapter);
        displayimage = (ImageView) findViewById(R.id.displayimage);
        textView_name = (TextView) findViewById(R.id.textView_name);
        textView_price = (TextView) findViewById(R.id.textView_price);
        locations = (TextView) findViewById(R.id.location);
        getLocationBtn = (Button) findViewById(R.id.getLocationBtn);
        addtocartbtn = (Button) findViewById(R.id.addtocart);
        addtocartbtn.setOnClickListener(this);

        Intent intent = getIntent();
        if (intent.getExtras() != null) {


            Intent intentt = this.getIntent();
            Bundle bundle = intentt.getExtras();

            ViewItemsPoJo viewItemsPoJoList =
                    (ViewItemsPoJo) bundle.getSerializable("value");
            viewItemsPoJo = viewItemsPoJoList;

            Glide.with(this)
                    .load(viewItemsPoJoList.getImage())
                    .thumbnail(0.5f)
                    .crossFade()
                    .placeholder(R.mipmap.ic_launcher)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(displayimage);

            locations.setText(viewItemsPoJoList.getLocation_name());
            textView_price.setText(viewItemsPoJoList.getPrice().toString());
            textView_name.setText(viewItemsPoJoList.getItem_name());

            Log.d("Extra", String.valueOf(displayimage) + "+++++++++++");
        }
        getLocationBtn.setOnClickListener(this);
        Toolbar mToolbar = (Toolbar) findViewById(R.id.item_name_toolbar);



        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        mToolbar.setTitle("GUCCI");

        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                onBackPressed();
                Intent i = new Intent(ItemDetailsActivity.this, ItemDetailsActivity.class);
                finish();

            }
        });


        // CircleIndicator indicator = (CircleIndicator) findViewById(R.id.circleIndicator);


        size_spinner = (Spinner) findViewById(R.id.size_spinner);
        color_spinner = (Spinner) findViewById(R.id.color_spinner);

        String MyString1 = "Medium";

        color = getResources().getStringArray(R.array.size);
        ArrayAdapter<String> ad = new ArrayAdapter<String>(ItemDetailsActivity.this, android.R.layout.simple_spinner_dropdown_item, color);

        spinner_position = ad.getPosition(MyString1);
        color_spinner.setAdapter(ad);

        ad.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);


        String[] colorname = {"Blue", "Red", "Pink", "Purple"};
        int[] colorimage = {R.color.wishlist, R.color.colorPrimary1, R.color.colorPrimary1, R.color.instergram};
        // GridLayout gridLayout1 = (GridLayout) findViewById(R.id.size_layout_container);

        Adapter adapter = new CustomSpineerAdaper(this, colorname, colorimage);
        size_spinner.setAdapter((SpinnerAdapter) adapter);

        size_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()

                                               {
                                                   @Override
                                                   public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                                                   }

                                                   @Override
                                                   public void onNothingSelected(AdapterView<?> adapterView) {

                                                   }
                                               }
        );
    }

    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.getLocationBtn:

                Intent myIntent = new Intent(view.getContext(), MapsActivity.class);
                Bundle bundle = new Bundle();
                bundle.putSerializable("value",viewItemsPoJo );
                myIntent.putExtras(bundle);
               startActivity(myIntent);

                break;

            case R.id.addtocart:
                CategoriesActivity.notificationCountCart++;
                ImageUrlUtils imageUrlUtils = new ImageUrlUtils();
                imageUrlUtils.addCartListImageUri(viewItemsPoJo);
                Toast.makeText(ItemDetailsActivity.this,"Item added to cart.",Toast.LENGTH_SHORT).show();
                break;
        }

    }

    }


