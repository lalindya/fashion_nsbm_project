package com.testapp.fashionwardrobe.wardrobe.DataClass;
import android.content.Context;
import com.testapp.fashionwardrobe.wardrobe.Database.DatabaseHelper;
import com.testapp.fashionwardrobe.wardrobe.main.LoginActivity;

public class DatabaseData {
    private DatabaseHelper myDB;

    public DatabaseData(LoginActivity loginActivity) {
    }


    public void InsertDataIfEmpty( Context context)
    {

        myDB = new DatabaseHelper(context);
        boolean ct1 = myDB.Cate01DataVerification();
        if(ct1==false)
        {
            myDB.insertCate01Data("Kids");
            myDB.insertCate01Data("Women");
            myDB.insertCate01Data("Men");
        }

        boolean ct2 = myDB.Cate02DataVerification();
        if(ct2==false)
        {
            myDB.insertCate02Data("Tshirts");
            myDB.insertCate02Data("Trousers");
            myDB.insertCate02Data("Shoes");
        }

        boolean ite = myDB.ItemsDataVerification();
        if(ite==false)
        {
            myDB.insertItemData("Tshirt Kids Summer",1500.00,"ODEL",6.907712,79.851001,"https://ae01.alicdn.com/kf/HTB18pcVLpXXXXa4XpXXq6xXFXXXm/Kids-T-Shirt-Boys-Girls-Clothes-2017-Summer-Fashion-Boys-Girls-Tshirt-Kids-Clothes-Brand-Car.jpg",1,1);
            myDB.insertItemData("Silky Tshirt",1400.00,"House of Fashions",6.907712,79.851001,"https://rlv.zcache.com/girls_ringer_t_shirt-r3b69a49127dc4e45bf32aace01898da1_k2l72_324.jpg",1,1);
            myDB.insertItemData("Sunrise New Tshirt",2000.00,"NOLIMITS",6.907712,79.851001,"https://cdn.supadupa.me/shop/45743/images/2815775/IMG_1216_large.jpg?1520347733",1,1);
            myDB.insertItemData("Young Buddy Tshirt",1800.00,"KANDY",6.907712,79.851001,"https://img.etsystatic.com/il/00375f/1017794291/il_570xN.1017794291_50pe.jpg?version=1",1,1);

            myDB.insertItemData("Superstretch Denim Leggings",1700.00,"Up Town Kandy",6.907712,79.851001,"http://lp.hm.com/hmprod?set=source%5B%2Fenvironment%2F2017%2F10G_0578_006R.jpg%5D%2Cmedia_type%5BFASHION_FRONT%5D%2Ctshirt_size%5BXL%5D%2Cquality%5BH%5D%2Csr_x%5B0%5D%2Csr_y%5B-1%5D%2Csr_height%5B4917%5D%2Csr_width%5B4205%5D%2Chmver%5B0%5D&call=url%5Bfile%3A%2Flegacy%2Fv1%2Fproduct.chain%5D",1,2);
            myDB.insertItemData("Denim Leggings",2000.00,"ODEL",6.907712,79.851001,"http://lp.hm.com/hmprod?set=source%5B%2Fenvironment%2F2018%2FH00_0000_22f9dee6445c4fe7acc6a5bb37664f9a0a020242.jpg%5D%2Cmedia_type%5BLOOKBOOK%5D%2Ctshirt_size%5BL%5D%2Cquality%5BH%5D%2Csr_x%5B-327%5D%2Csr_y%5B0%5D%2Csr_height%5B3496%5D%2Csr_width%5B2990%5D%2Chmver%5B0%5D&call=url%5Bfile%3A%2Fstudio2%2Fv1%2Fproduct.chain%5D",1,2);
            myDB.insertItemData("Jersey Treggings",2200.00,"House of Fashions",6.907712,79.851001,"http://lp.hm.com/hmprod?set=source%5B%2Fenvironment%2F2018%2FG00_0000_93a67adb008541a44a743f9d34fedcf6ca63abc1.jpg%5D%2Cmedia_type%5BLOOKBOOK%5D%2Ctshirt_size%5BL%5D%2Cquality%5BH%5D%2Csr_x%5B-327%5D%2Csr_y%5B0%5D%2Csr_height%5B3496%5D%2Csr_width%5B2990%5D%2Chmver%5B0%5D&call=url%5Bfile%3A%2Fstudio2%2Fv1%2Fproduct.chain%5D",1,2);
            myDB.insertItemData("2-pack Treggings",1900.00,"Cotton Collection",6.907712,79.851001,"http://lp.hm.com/hmprod?set=source%5B%2Fenvironment%2F2018%2FG00_0000_4c0a1011a34141737c33bc34b42caa5ead5d85f6.jpg%5D%2Cmedia_type%5BLOOKBOOK%5D%2Ctshirt_size%5BL%5D%2Cquality%5BH%5D%2Csr_x%5B-327%5D%2Csr_y%5B0%5D%2Csr_height%5B3496%5D%2Csr_width%5B2990%5D%2Chmver%5B0%5D&call=url%5Bfile%3A%2Fstudio2%2Fv1%2Fproduct.chain%5D",1,2);

            myDB.insertItemData("Under Armour Kids",2500.00,"NOLIMITS",6.907712,79.851001,"https://www.adidas.co.uk/dis/dw/image/v2/aagl_prd/on/demandware.static/-/Sites-adidas-products/default/dwe5a05c8c/zoom/CQ1769_01_standard.jpg?sh=840&strip=false&sw=840",1,3);
            myDB.insertItemData("Primigi Kids",1400.00,"KANDY",6.907712,79.851001,"https://www.adidas.co.uk/dis/dw/image/v2/aagl_prd/on/demandware.static/-/Sites-adidas-products/default/dwb48ef154/zoom/CQ2925_01_standard.jpg?sh=840&strip=false&sw=840",1,3);
            myDB.insertItemData("New Balance Kids KL574v1 ",2700.00,"House of Fashions",6.907712,79.851001,"https://www.adidas.co.uk/dis/dw/image/v2/aagl_prd/on/demandware.static/-/Sites-adidas-products/default/dwcc0efa1b/zoom/AC8544_01_standard.jpg?sh=840&strip=false&sw=840",1,3);
            myDB.insertItemData("New Balance Kids KA680v3",1800.00,"ODEL",6.907712,79.851001,"https://www.adidas.co.uk/dis/dw/image/v2/aagl_prd/on/demandware.static/-/Sites-adidas-products/default/dwed52dac1/zoom/CQ2361_01_standard.jpg?sh=840&strip=false&sw=840",1,3);

            myDB.insertItemData("Kyra Graff Shirt",1300.00,"House of Fashions",6.907712,79.851001,"https://images.allsaints.com/products/600/WH090N/5/WH090N-5-2.jpg",2,1);
            myDB.insertItemData("Selima Ebooshi Top",1400.00,"ODEL",6.907712,79.851001,"https://images.allsaints.com/products/600/WM246N/4068/WM246N-4068-2.jpg",2,1);
            myDB.insertItemData("Cropped Cannon Shirt",2000.00,"Glitz",6.907712,79.851001,"https://images.allsaints.com/products/600/WH097N/2824/WH097N-2824-2.jpg",2,1);
            myDB.insertItemData("Annie Nomy Top",1800.00,"NOLIMITS",6.907712,79.851001,"https://images.allsaints.com/products/600/WM265N/5039/WM265N-5039-2.jpg",2,1);
            myDB.insertItemData("Pome Bay Shirt",1800.00,"KANDY",6.907712,79.851001,"https://images.allsaints.com/products/600/WH785N/2824/WH785N-2824-2.jpg",2,1);

            myDB.insertItemData("LOLA CROPPED JEAN",1500.00,"Glitz",6.907712,79.851001,"https://images.allsaints.com/products/600/WE041N/4119/WE041N-4119-1.jpg",2,2);
            myDB.insertItemData("GRACE SKINNY JEAN",1400.00,"House of Fashions",6.907712,79.851001,"https://images.allsaints.com/products/600/WE082N/162/WE082N-162-1.jpg",2,2);
            myDB.insertItemData("STILT VINTAGE SKINNY JEANS",2000.00,"NOLIMITS",6.907712,79.851001,"https://images.allsaints.com/products/600/WE040N/103/WE040N-103-1.jpg",2,2);
            myDB.insertItemData("BIKER ANKLE SKINNY JEANS",1800.00,"Cotton Collection",6.907712,79.851001,"https://images.allsaints.com/products/600/WE066N/2824/WE066N-2824-1.jpg",2,2);

            myDB.insertItemData("COURTNEE SANDAL",1500.00,"Cotton Collection",6.907712,79.851001,"https://www.adidas.co.uk/dis/dw/image/v2/aagl_prd/on/demandware.static/-/Sites-adidas-products/default/dwec32b0b5/zoom/CQ2235_01_standard.jpg?sh=840&strip=false&sw=840",2,3);
            myDB.insertItemData("SARSINA PUMP",1400.00,"KANDY",6.907712,79.851001,"https://www.adidas.co.uk/dis/dw/image/v2/aagl_prd/on/demandware.static/-/Sites-adidas-products/default/dw28cc026b/zoom/280648_01_standard.jpg?sh=840&strip=false&sw=840",2,3);
            myDB.insertItemData("EZABELLA SANDAL",2000.00,"ODEL",6.907712,79.851001,"https://www.adidas.co.uk/dis/dw/image/v2/aagl_prd/on/demandware.static/-/Sites-adidas-products/default/dwc4256e5d/zoom/CQ2234_01_standard.jpg?sh=840&strip=false&sw=840",2,3);
            myDB.insertItemData("SERINA SANDAL",1800.00,"Glitz",6.907712,79.851001,"https://www.adidas.co.uk/dis/dw/image/v2/aagl_prd/on/demandware.static/-/Sites-adidas-products/default/dw8ff88a28/zoom/BA7539_01_standard.jpg?sh=840&strip=false&sw=840",2,3);

            myDB.insertItemData("BRACE TONIC CREW T-SHIRT",1500.00,"Glitz",6.907712,79.851001,"https://images.allsaints.com/products/900/MD131G/5251/MD131G-5251-1.jpg",3,1);
            myDB.insertItemData("CLASH CREW T-SHIRT",1400.00,"House of Fashions",6.907712,79.851001,"https://images.allsaints.com/products/900/MD050M/4098/MD050M-4098-1.jpg",3,1);
            myDB.insertItemData("HALL STRIPE CREW",2000.00,"ODEL",6.907712,79.851001,"https://images.allsaints.com/products/900/MD038N/5271/MD038N-5271-1.jpg",3,1);
            myDB.insertItemData("CRADLE CREW T-SHIRT",1800.00,"KANDY",6.907712,79.851001,"https://images.allsaints.com/products/900/MD063N/5250/MD063N-5250-1.jpg",3,1);

            myDB.insertItemData("INKA REED STRAIGHT JEAN",1500.00,"ODEL",6.907712,79.851001,"https://images.allsaints.com/products/900/ME009N/21/ME009N-21-1.jpg",3,2);
            myDB.insertItemData("IOWA REED STRAIGHT JEAN",1400.00,"Glitz",6.907712,79.851001,"https://images.allsaints.com/products/900/ME007N/21/ME007N-21-1.jpg",3,2);
            myDB.insertItemData("BODMIN REED STRAIGHT JEAN",2000.00,"NOLIMITS",6.907712,79.851001,"https://images.allsaints.com/products/900/ME021N/5/ME021N-5-1.jpg",3,2);
            myDB.insertItemData("GRAINE REED STRAIGHT JEAN",1800.00,"Cotton Collection",6.907712,79.851001,"https://images.allsaints.com/products/900/ME008N/7/ME008N-7-1.jpg",3,2);

            myDB.insertItemData("Oxfords Blue Shoes",1500.00,"Cotton Collection",6.907712,79.851001,"https://www.adidas.co.uk/dis/dw/image/v2/aagl_prd/on/demandware.static/-/Sites-adidas-products/default/dw5359ffd2/zoom/BA8019_01_standard.jpg?sh=840&strip=false&sw=840",3,3);
            myDB.insertItemData("Suede Shoes",1400.00,"House of Fashions",6.907712,79.851001,"https://www.adidas.co.uk/dis/dw/image/v2/aagl_prd/on/demandware.static/-/Sites-adidas-products/default/dw3280b175/zoom/CQ1772_01_standard.jpg?sh=840&strip=false&sw=840",3,3);
            myDB.insertItemData("Leatherette Spring Oxfords Brown shoes",2000.00,"ODEL",6.907712,79.851001,"https://www.adidas.co.uk/dis/dw/image/v2/aagl_prd/on/demandware.static/-/Sites-adidas-products/default/dwe5a05c8c/zoom/CQ1769_01_standard.jpg?sh=840&strip=false&sw=840",3,3);
            myDB.insertItemData("Oxfords Black Winter Formal Shoes",1800.00,"Glitz",6.907712,79.851001,"https://www.adidas.co.uk/dis/dw/image/v2/aagl_prd/on/demandware.static/-/Sites-adidas-products/default/dw2051f732/zoom/B22681_01_standard.jpg?sh=840&strip=false&sw=840",3,3);
        }
    }



}
